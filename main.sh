#!/bin/bash
path=$(pwd)
cd /root && mkdir temp-backups && cd $path
cd $path
./backup-utils.sh
cd /root/temp-backups && rm -rf * && cd $path
./backup-sql.sh
cd /root/temp-backups && rm -rf * && cd $path
./backup-sites.sh
echo " Backup Finished!"
cd /root && rm -rf temp-backups

