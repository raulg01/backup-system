#!/bin/bash
read -d $'\x04' path <"config/path"
read -d $'\x04' remoteip <"config/ip"
read -d $'\x04' webserver <"config/webserver"
read -d $'\x04' user <"config/user"
today=$(date '+%d.%m.%y')
bloc="/home/backup/$webserver/web/$today"
cd /root && mkdir temp-backups
sshpass -p " " ssh $user@$remoteip 'cd /home/backup/$webserver/web && mkdir $(date '+%d.%m.%y')'
dir="/root/temp-backups/"
cd $path
counter=0
for D in `ls`
do
	backupname=$D
	backupname+=".tar"
	tar -czvf $backupname $D
    	mv $backupname $dir
	cd $dir
	cd $backupname
	rm -rf cache
	mkdir cache
	cd $dir
	rsync -rpazvhue 'sshpass -p "" ssh ' $dir/$backupname  $user@$remoteip:$bloc
	rm -rf $backupname
	cd $path
	echo "Backup finished for $D"
	counter=$((counter+1))
done
cd /root
rm -rf temp-backups
echo "Backup Finished!"







   
  

